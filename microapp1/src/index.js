import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import "@informatica/droplets-core/dist/themes/archipelago/archipelago.css";

ReactDOM.render(<App />, document.getElementById("microapp1"));
