
import React, { useEffect } from "react";
import { Shell } from "@informatica/droplets-core";

const Micro = () => {
    useEffect(() => {
        const script = document.createElement('script');
        script.src = "./childmain.js";
        script.async = true;
        document.body.appendChild(script);
        return () => {
            document.body.removeChild(script);
        }
    }, []);

    return (
        <Shell.Page>
            <div id="microapp"></div>
        </Shell.Page>
    );
};

export const MicroApp = {
    path: "/micro",
    component: Micro,
    preserveState: "children",
    meta: {
        nav: {
            label: 'Micro app'
        }
    }
};
