import React from "react";
import { Shell } from '@informatica/droplets-common';
const InnerPage = React.lazy(() => import('./Inner.js'));

const InnerApp = () => {
    return (
        <Shell.Page>
            < React.Suspense fallback={<div>Cannot load inner app</div>}>
                <InnerPage />
            </ React.Suspense>
        </Shell.Page>

    );
}

export const InnerAppContainer = {
    path: "/inner",
    component: InnerApp,
    preserveState: "children",
    meta: {
        nav: {
            label: 'Inner app'
        }
    }
};
