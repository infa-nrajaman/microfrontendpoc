
import React from "react";
import { Shell, Panel } from "@informatica/droplets-core";

const HomeApp = () => {
    return (
        <Shell.Page>
            <Panel>Home</Panel>
        </Shell.Page>
    );
};

export const Home = {
    path: "/",
    component: HomeApp,
    preserveState: "children",
    meta: {
        nav: {
            label: 'Home'
        }
    }
};
