import { MicroApp } from './MicroApp';
import { MicroApp1 } from './MicroApp1';
import { InnerAppContainer } from './InnerAppContainer';
import { Home } from './Home';
export const routes = [Home, InnerAppContainer, MicroApp, MicroApp1];