import React, { useState } from "react";
import { ExpandContainer, useExpandState, Panel } from "@informatica/droplets-core";

const Inner = () => {
    const expand = useExpandState();
    const [dataToChild, setDataToChild] = useState('');
    const [dataFromChild, setDataFromChild] = useState('');
    document.addEventListener('childSending', (val) => setDataFromChild(val.detail));
    const handleChange = (e) => {
        let data = e.target.value;
        document.dispatchEvent(new CustomEvent('parentSending', { detail: data }));
        setDataToChild(data);
    }
    return (
        <Panel title='Inner app'>
            <Panel title='Droplets r35 expand container'>
                <ExpandContainer {...expand} label={`Show ${expand.expanded ? "Less" : "More"}`}>
                    <p>Expanded content </p>
                </ExpandContainer>
            </Panel>
            <Panel title='Interapp communication with events'>
                <div> Enter data to relay to micro app -  <input value={dataToChild} onChange={handleChange} />
                    <div>Data from micro app = {dataFromChild}</div></div>
            </Panel>
        </Panel>

    );
}
export default Inner;

