
import React, { useEffect } from "react";
import { Shell } from "@informatica/droplets-core";

const Micro1 = () => {
    useEffect(() => {
        const script = document.createElement('script');
        script.src = "./childmain1.js";
        script.async = true;
        document.body.appendChild(script);
        return () => {
            document.body.removeChild(script);
        }
    }, []);

    return (
        <Shell.Page>
            <div id="microapp1"></div>
        </Shell.Page>
    );
};

export const MicroApp1 = {
    path: "/micro1",
    component: Micro1,
    preserveState: "children",
    meta: {
        nav: {
            label: 'Micro app 1'
        }
    }
};
