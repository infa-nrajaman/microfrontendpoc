import React from "react";
import { Shell } from '@informatica/droplets-common';
import { routes } from './Routes';

export const App = () => {
  return <Shell style={{ width: "100%" }} productName="App"
    routes={[...routes]}
    basePath="/"
    extensibility={"true"}>
    <Shell.Header productName="App"></Shell.Header>
    <Shell.Nav />
    <Shell.Main />
  </Shell>
}
