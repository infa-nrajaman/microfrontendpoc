import React, { useState } from "react";
import { Table, ExpandContainer, useExpandState, Panel } from '@informatica/droplets-core';

export default function App() {
  const expand = useExpandState();
  const [dataToParent, setDataToParent] = useState('');
  const [dataFromParent, setDataFromParent] = useState('');
  document.addEventListener('parentSending', (val) => setDataFromParent(val.detail));
  const handleChange = (e) => {
    let data = e.target.value;
    document.dispatchEvent(new CustomEvent('childSending', { detail: data }));
    setDataToParent(data);
  }
  return (
    <Panel title='Micro app'>
      <Panel title='Droplets r35 expand container'>
        <ExpandContainer {...expand} label={`Show ${expand.expanded ? "Less" : "More"}`}>
          <p>Expanded content </p>
        </ExpandContainer>
      </Panel>
      <Panel title='Droplets r35 table'>
        <Table>
          <Table.Header>
            <Table.HeaderRow>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
            </Table.HeaderRow>
          </Table.Header>
          <Table.Body>
            <Table.Row key={1}>
              <Table.Cell>testname1</Table.Cell>
              <Table.Cell>testemail1</Table.Cell>
            </Table.Row>
            <Table.Row key={2}>
              <Table.Cell>testname2</Table.Cell>
              <Table.Cell>testemail2</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </Panel>
      <Panel title='Interapp communication with events'>
        <div>Enter data to relay to external app -  <input value={dataToParent} onChange={handleChange} />
          <div>Data from external app = {dataFromParent}</div></div>
      </Panel>
    </Panel>
  );
}

